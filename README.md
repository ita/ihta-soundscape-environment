# Introduction

Soundscape research demands a holistic approach for the analysis of environments, yet any research method, e.g. soundwalk or lab-based listening test, has its advantages and limitations. The virtual reality (VR) and augmented reality (AR) technology provides an alternative method for soundscape research, which may remain as much context as possible while enable control conditions. The current phase of this research project creates a set of virtual environments, to study the ecological validity of virtual reality environment for soundscape, by comparison of human experiences and responses in a real soundscape and three-dimensional virtual ones. The workflow of scene rendering on different levels of detail is described. The results are made publicly available.

# Structure and Details


### Videos

A recorded video of the scene for each setting. The settings are as follows.

* **AR scene**
based on the recorded scene and artificially added a walking person from another recording.

[![AR scene](https://img.youtube.com/vi/creZXfprCe0/0.jpg)](https://www.youtube.com/watch?v=creZXfprCe0)

* **Audio only scene**
no visuals, recording is based on auralisation simulation.

[![Audio Only](https://img.youtube.com/vi/vdi9OZ7UWHY/0.jpg)](https://www.youtube.com/watch?v=vdi9OZ7UWHY)

* **Recorded scene**
recorded scene without any special sounds and visuals.

[![Recorded scene](https://img.youtube.com/vi/PS1MZss018k/0.jpg)](https://www.youtube.com/watch?v=PS1MZss018k)

* **VR high resolution scene**
based on 3D virtual geometric model, including auralisation simulation.

[![VR high resolution](https://img.youtube.com/vi/YzK7nCiSJkI/0.jpg)](https://www.youtube.com/watch?v=YzK7nCiSJkI)

* **VR medium resolution scene**
based on 3D virtual geometric model, including auralisation simulation.

[![VR medium resolution](https://img.youtube.com/vi/ND_HdfFgR3I/0.jpg)](https://www.youtube.com/watch?v=ND_HdfFgR3I)

* **VR simple scene**
based on 3D virtual geometric model with low resolution and visualisation details, including auralisation simulation.

[![VR simple resolution](https://img.youtube.com/vi/Xjw6txeg9zc/0.jpg)](https://www.youtube.com/watch?v=Xjw6txeg9zc)


# Literatur

This data is provided in connection with the following publication.

*Heimes, A., Yang, M., & Vorländer, M. (2021). Virtual reality environments for soundscape research. proceeding of 1st Conference on Sound Perception (CSP).*
